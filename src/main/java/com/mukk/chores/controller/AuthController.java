package com.mukk.chores.controller;

import com.mukk.chores.dto.BasicUserDTO;
import com.mukk.chores.dto.TokenDTO;
import com.mukk.chores.model.request.LoginDetails;
import com.mukk.chores.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<TokenDTO> login(@RequestBody @NonNull LoginDetails details) {
        return authService.login(details);
    }

    @GetMapping("/current")
    public ResponseEntity<BasicUserDTO> getCurrentUser() {
        return authService.getCurrentUser();
    }
}
