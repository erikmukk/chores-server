package com.mukk.chores.controller;

import com.mukk.chores.dto.BasicUserDTO;
import com.mukk.chores.dto.TeamDTO;
import com.mukk.chores.model.request.TeamCreationDetails;
import com.mukk.chores.model.request.TeamJoinDetails;
import com.mukk.chores.model.request.UserApprovalDetails;
import com.mukk.chores.service.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/team")
public class TeamController {
    private final TeamService teamService;

    @GetMapping("/{teamID}")
    public ResponseEntity<TeamDTO> getById(@PathVariable(value = "teamID") Long teamID) {
        return teamService.findById(teamID);
    }

    @GetMapping("/{teamId}/allUsers")
    public ResponseEntity<List<BasicUserDTO>> getAllUsersInTeam(@PathVariable(value = "teamId") Long teamId) {
        return teamService.getAllUsersInTeam(teamId);
    }

    @GetMapping("/currentUser")
    public ResponseEntity<List<TeamDTO>> getById() {
        return teamService.findCurrentUserTeams();
    }

    @GetMapping("/search")
    public ResponseEntity<List<TeamDTO>> search(@RequestParam(name = "searchTerm") String searchTerm) {
        return teamService.search(searchTerm);
    }

    @PostMapping
    public ResponseEntity<TeamDTO> create(@RequestBody @NonNull TeamCreationDetails details) {
        return teamService.create(details);
    }

    @PutMapping("/{teamId}/join")
    public ResponseEntity<TeamDTO> joinTeam(@RequestBody @NonNull TeamJoinDetails details, @PathVariable(value = "teamId") Long teamID) {
        return teamService.join(details, teamID);
    }

    @PutMapping("/{teamId}/leave")
    public ResponseEntity<TeamDTO> leaveTeam(@RequestBody @NonNull TeamJoinDetails details, @PathVariable(value = "teamId") Long teamID) {
        return teamService.leave(details, teamID);
    }

    @PostMapping("/approveUser")
    public ResponseEntity<HttpStatus> approveUser(@RequestBody @NonNull UserApprovalDetails details) {
        return teamService.approveUser(details);
    }

    @GetMapping("/one")
    public ResponseEntity<TeamDTO> getUserByUuid(@RequestParam(name = "uuid") String uuid) {
        return teamService.getByUuid(uuid);
    }
}
