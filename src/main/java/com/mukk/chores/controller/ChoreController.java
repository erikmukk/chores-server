package com.mukk.chores.controller;

import com.mukk.chores.dto.ChoreDTO;
import com.mukk.chores.model.request.ChoreCreationDetails;
import com.mukk.chores.model.request.ChoreEditDetails;
import com.mukk.chores.service.ChoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/chore")
public class ChoreController {

    private final ChoreService choreService;

    @GetMapping
    public ResponseEntity<List<ChoreDTO>> getAll() {
        return choreService.getAll();
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<ChoreDTO>> getUserChores(@PathVariable(value = "userId") Long userId) {
        return choreService.getUserChores(userId);
    }

    @GetMapping("/team/{teamId}")
    public ResponseEntity<List<ChoreDTO>> getTeamChores(@PathVariable(value = "teamId") Long teamId) {
        return choreService.getTeamChores(teamId);
    }

    @PostMapping
    public ResponseEntity<ChoreDTO> create(@RequestBody @NonNull ChoreCreationDetails details) {
        return choreService.create(details);
    }

    @GetMapping("/{choreID}")
    public ResponseEntity<ChoreDTO> getById(@PathVariable(value="choreID") Long id) {
        return choreService.getById(id);
    }

    @PutMapping("/{choreID}")
    public ResponseEntity<ChoreDTO> edit(@PathVariable(value = "choreID") Long id, @RequestBody ChoreEditDetails details) {
        return choreService.edit(id, details);
    }

    @DeleteMapping("/{choreID}")
    public ResponseEntity<ChoreDTO> delete(@PathVariable(value = "choreID") Long id) {
        return choreService.delete(id);
    }
}
