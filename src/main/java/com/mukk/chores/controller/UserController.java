package com.mukk.chores.controller;

import com.mukk.chores.dto.BasicUserDTO;
import com.mukk.chores.dto.ChoreDTO;
import com.mukk.chores.dto.UserDTO;
import com.mukk.chores.model.request.UserCreationDetails;
import com.mukk.chores.service.AuthService;
import com.mukk.chores.service.ChoreService;
import com.mukk.chores.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;
    private final ChoreService choreService;
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll() {
        return userService.getAll();
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> createUser(@RequestBody @NonNull UserCreationDetails details) {
        return authService.create(details);
    }

    @GetMapping("/{userID}")
    public ResponseEntity<UserDTO> getUserByID(@PathVariable(value="userID") Long id) {
        return userService.getByID(id);
    }

    @GetMapping("/one")
    public ResponseEntity<BasicUserDTO> getUserByUuid(@RequestParam(name = "uuid") String uuid) {
        return userService.getByUuid(uuid);
    }

    // ?assignedTo=true&createdByBy=true
    @GetMapping("/{username}/chores")
    public ResponseEntity<List<ChoreDTO>> getChoresForUser(@PathVariable(value="username") String username,
                                                           @RequestParam(required = false, name = "assignedTo") boolean assignedTo,
                                                           @RequestParam(required = false, name = "createdBy") boolean createdBy) {
        return choreService.getChores(username, assignedTo, createdBy);
    }
}
