package com.mukk.chores.repository;

import com.mukk.chores.model.Team;
import com.mukk.chores.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    List<Team> findTeamByMembersContaining(User user);

    List<Team> findAllByNameContaining(String searchTerm);

    Optional<Team> findByUuid(String uuid);
}
