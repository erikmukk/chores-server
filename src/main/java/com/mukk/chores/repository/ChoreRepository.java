package com.mukk.chores.repository;

import com.mukk.chores.model.Chore;
import com.mukk.chores.model.Team;
import com.mukk.chores.model.User;
import com.mukk.chores.model.enums.ChoreStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ChoreRepository extends JpaRepository<Chore, Long> {

    List<Chore> findByAssignedTo(User assignedTo);

    List<Chore> findByCreatedBy(User createdBy);

    List<Chore> findByAssignedToAndChoreStatus(User user, ChoreStatus choreStatus);

    List<Chore> findByTargetTeam(Team team);
}
