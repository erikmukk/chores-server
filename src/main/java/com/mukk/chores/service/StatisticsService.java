package com.mukk.chores.service;

import com.mukk.chores.dto.StatisticsDTO;
import com.mukk.chores.model.Chore;
import com.mukk.chores.model.Team;
import com.mukk.chores.model.User;
import com.mukk.chores.model.enums.ChoreStatus;
import com.mukk.chores.repository.ChoreRepository;
import com.mukk.chores.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StatisticsService {
    private final ChoreRepository choreRepository;
    private final TeamRepository teamRepository;
    private final ObjectService objectService;

    public ResponseEntity<StatisticsDTO> getStatisticsByUserId() {
        String authedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = objectService.getUserByUsername(authedUser);
        List<Chore> completedChores = choreRepository.findByAssignedToAndChoreStatus(user, ChoreStatus.COMPLETED);
        List<Chore> inProgressChores = choreRepository.findByAssignedToAndChoreStatus(user, ChoreStatus.STARTED);
        List<Chore> assignedChores = choreRepository.findByAssignedTo(user);
        List<Chore> createdChores = choreRepository.findByCreatedBy(user);
        List<Team> teams = teamRepository.findTeamByMembersContaining(user);
        int totalChores = completedChores.size() + createdChores.size();
        StatisticsDTO statisticsDTO = new StatisticsDTO(totalChores, completedChores.size(), inProgressChores.size(), createdChores.size(), assignedChores.size(), teams.size());
        return new ResponseEntity<>(statisticsDTO, HttpStatus.OK);
    }
}
