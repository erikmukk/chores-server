package com.mukk.chores.service;

import com.mukk.chores.dto.ChoreDTO;
import com.mukk.chores.exception.ResourceException;
import com.mukk.chores.model.Chore;
import com.mukk.chores.model.Team;
import com.mukk.chores.model.User;
import com.mukk.chores.model.request.ChoreCreationDetails;
import com.mukk.chores.model.request.ChoreEditDetails;
import com.mukk.chores.repository.ChoreRepository;
import com.mukk.chores.util.ObjectMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ChoreService {
    private final ChoreRepository choreRepository;
    private final ObjectService objectService;

    public ResponseEntity<List<ChoreDTO>> getAll() {
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(choreRepository.findAll(), ChoreDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<ChoreDTO> create(ChoreCreationDetails details) {
        String authedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        User assignedTo = objectService.getUserById(details.getAssignedTo());
        User createdBy = objectService.getUserByUsername(authedUser);
        Team targetTeam = objectService.getTeamById(details.getTargetTeam());
        Chore chore = Chore.builder()
                .assignedTo(assignedTo)
                .createdBy(createdBy)
                .name(details.getName())
                .description(details.getDescription())
                .description(details.getDescription())
                .targetTeam(targetTeam)
                .build();
        return new ResponseEntity<>(ObjectMapperUtil.map(choreRepository.save(chore), ChoreDTO.class), HttpStatus.CREATED);
    }

    public ResponseEntity<List<ChoreDTO>> getChores(String username, boolean findByAssigned, boolean findByCreatedBy) {
        String authedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!authedUser.equals(username)) {
            throw new ResourceException(HttpStatus.FORBIDDEN, String.format("You have no rights to view chores of user '%s'", username));
        }

        User user = objectService.getUserByUsername(username);
        List<Chore> chores = new ArrayList<>();
        if (findByAssigned) {
            chores.addAll(choreRepository.findByAssignedTo(user));
        }
        if (findByCreatedBy) {
            chores.addAll(choreRepository.findByCreatedBy(user));
        }
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(chores, ChoreDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<ChoreDTO> getById(Long id) {
        Chore chore = objectService.getChoreById(id);
        return new ResponseEntity<>(ObjectMapperUtil.map(chore, ChoreDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<ChoreDTO> edit(Long id, ChoreEditDetails details) {
        Chore chore = objectService.getChoreById(id);

        if (details.getName() != null) {
            chore.setName(details.getName());
        }
        if (details.getDescription() != null) {
            chore.setDescription(details.getDescription());
        }
        if (details.getChoreStatus() != null) {
            chore.setChoreStatus(details.getChoreStatus());
        }
        if (details.getAssignedTo() != null) {
            User user = objectService.getUserById(details.getAssignedTo());
            chore.setAssignedTo(user);
        }
        return new ResponseEntity<>(ObjectMapperUtil.map(choreRepository.save(chore), ChoreDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<List<ChoreDTO>> getUserChores(Long userId) {
        User user = objectService.getUserById(userId);
        List<Chore> chores = choreRepository.findByAssignedTo(user);
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(chores, ChoreDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<List<ChoreDTO>> getTeamChores(Long teamId) {
        Team team = objectService.getTeamById(teamId);
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(choreRepository.findByTargetTeam(team), ChoreDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<ChoreDTO> delete(Long id) {
        choreRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
