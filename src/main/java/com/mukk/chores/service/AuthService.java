package com.mukk.chores.service;

import com.mukk.chores.dto.BasicUserDTO;
import com.mukk.chores.dto.TokenDTO;
import com.mukk.chores.dto.UserDTO;
import com.mukk.chores.exception.ResourceException;
import com.mukk.chores.model.User;
import com.mukk.chores.model.request.LoginDetails;
import com.mukk.chores.model.request.UserCreationDetails;
import com.mukk.chores.repository.UserRepository;
import com.mukk.chores.util.JWTUtil;
import com.mukk.chores.util.ObjectMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AuthService {
    private final UserRepository userRepository;
    private final ObjectService objectService;
    private final TeamService teamService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JWTUtil jwtUtil;
    private final EmailService emailService;

    public ResponseEntity<UserDTO> create(UserCreationDetails details) {
        Optional<User> existingUserEmail = userRepository.findUserByEmail(details.getEmail());
        if (existingUserEmail.isPresent()) {
            throw new ResourceException(HttpStatus.BAD_REQUEST, String.format("User with email %s already exists.", details.getEmail()));
        }
        Optional<User> existingUserUsername = userRepository.findByUsername(details.getUsername());
        if (existingUserUsername.isPresent()) {
            throw new ResourceException(HttpStatus.BAD_REQUEST, String.format("User with username %s already exists.", details.getUsername()));
        }
        User user = User.builder()
                .username(details.getUsername())
                .email(details.getEmail())
                .passwordHash(bCryptPasswordEncoder.encode(details.getPassword()))
                .firstName(details.getFirstName())
                .lastName(details.getLastName())
                .createdChores(new ArrayList<>())
                .assignedChores(new ArrayList<>())
                .build();
        teamService.createInitialTeamForUser(user);
        emailService.sendRegistrationMail(user.getEmail(), "Registration", "Your account has been created.");
        return new ResponseEntity<>(ObjectMapperUtil.map(userRepository.save(user), UserDTO.class), HttpStatus.CREATED);
    }

    public ResponseEntity<TokenDTO> login(LoginDetails details) {
        Optional<User> user = userRepository.findByUsername(details.getUsername());
        if (user.isEmpty()) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "Username or password is incorrect.");
        }
        if (bCryptPasswordEncoder.matches(details.getPassword(), user.get().getPasswordHash())) {
            TokenDTO authResponse = new TokenDTO(jwtUtil.generateToken(user.get()));
            return new ResponseEntity<>(authResponse ,HttpStatus.OK);
        }
        throw new ResourceException(HttpStatus.UNAUTHORIZED, "Username or password is incorrect.");
    }

    public ResponseEntity<BasicUserDTO> getCurrentUser() {
        String authedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = objectService.getUserByUsername(authedUser);
        return new ResponseEntity<>(ObjectMapperUtil.map(user, BasicUserDTO.class), HttpStatus.OK);
    }
}
