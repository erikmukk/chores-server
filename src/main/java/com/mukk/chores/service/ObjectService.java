package com.mukk.chores.service;

import com.mukk.chores.exception.ResourceException;
import com.mukk.chores.model.Chore;
import com.mukk.chores.model.Team;
import com.mukk.chores.model.User;
import com.mukk.chores.repository.ChoreRepository;
import com.mukk.chores.repository.TeamRepository;
import com.mukk.chores.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ObjectService {
    private final UserRepository userRepository;
    private final TeamRepository teamRepository;
    private final ChoreRepository choreRepository;

    public User getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("User with id %s not found", id));
        }
        return user.get();
    }

    public User getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("User with name %s not found", username));
        }
        return user.get();
    }

    public Team getTeamById(Long id) {
        Optional<Team> team = teamRepository.findById(id);
        if (team.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("Team with id %s not found", id));
        }
        return team.get();
    }

    public Chore getChoreById(Long id) {
        Optional<Chore> chore = choreRepository.findById(id);
        if (chore.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("Chore with id %s not found", id));
        }
        return chore.get();
    }

    public User findUserByEmail(String email) {
        Optional<User> user = userRepository.findUserByEmail(email);
        if (user.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("User with email %s not found", email));
        }
        return user.get();
    }

    public User getUserByJwt() {
        String authedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> user = userRepository.findByUsername(authedUser);
        if (user.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, "User with current JWT does not exist.");
        }
        return user.get();
    }

    public Team getTeamByUuid(String uuid) {
        Optional<Team> team = teamRepository.findByUuid(uuid);
        if (team.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("Team with uuid %s does not exist", uuid));
        }
        return team.get();
    }

    public User getUserByUuid(String uuid) {
        Optional<User> user = userRepository.findByUuid(uuid);
        if (user.isEmpty()) {
            throw new ResourceException(HttpStatus.NOT_FOUND, String.format("User with uuid %s does not exist", uuid));
        }
        return user.get();
    }
}
