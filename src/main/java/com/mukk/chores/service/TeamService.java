package com.mukk.chores.service;

import com.mukk.chores.dto.BasicUserDTO;
import com.mukk.chores.dto.TeamDTO;
import com.mukk.chores.exception.ResourceException;
import com.mukk.chores.model.Team;
import com.mukk.chores.model.User;
import com.mukk.chores.model.request.TeamCreationDetails;
import com.mukk.chores.model.request.TeamJoinDetails;
import com.mukk.chores.model.request.UserApprovalDetails;
import com.mukk.chores.repository.TeamRepository;
import com.mukk.chores.util.ObjectMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TeamService {
    private final TeamRepository teamRepository;
    private final EmailService emailService;
    private final ObjectService objectService;

    public ResponseEntity<TeamDTO> create(TeamCreationDetails details) {
        User createdBy = objectService.getUserByJwt();
        Team team = Team.builder()
                .name(details.getName())
                .createdBy(createdBy)
                .description(details.getDescription())
                .admin(createdBy)
                .members(Collections.singletonList(createdBy))
                .build();
        return new ResponseEntity<>(ObjectMapperUtil.map(teamRepository.save(team), TeamDTO.class), HttpStatus.CREATED);
    }

    public ResponseEntity<TeamDTO> findById(Long teamID) {
        return new ResponseEntity<>(ObjectMapperUtil.map(objectService.getTeamById(teamID), TeamDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<List<TeamDTO>> findCurrentUserTeams() {
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(
                teamRepository.findTeamByMembersContaining(objectService.getUserByJwt()), TeamDTO.class), HttpStatus.OK);
    }
    public ResponseEntity<List<BasicUserDTO>> getAllUsersInTeam(Long teamId) {
        Team team = objectService.getTeamById(teamId);
        List<User> users = team.getMembers();
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(users, BasicUserDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<List<TeamDTO>> search(String searchTerm) {
        if (searchTerm.equals("")) {
            return new ResponseEntity<>(ObjectMapperUtil.mapAll(new ArrayList<>(), TeamDTO.class), HttpStatus.OK);
        }
        List<Team> teams = teamRepository.findAllByNameContaining(searchTerm);
        teams = teams.stream().filter(team -> !team.getIsDefaultTeam()).collect(Collectors.toList());
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(teams, TeamDTO.class), HttpStatus.OK);
    }

    public void createInitialTeamForUser(User user) {
        Team team = Team.builder()
                .name(String.format("Default team for %s", user.getUsername()))
                .createdBy(user)
                .description("This is your default team")
                .isDefaultTeam(true)
                .admin(user)
                .members(Collections.singletonList(user))
                .build();
        teamRepository.save(team);
    }

    public ResponseEntity<TeamDTO> join(TeamJoinDetails details, Long teamId) {
        User user = objectService.getUserById(details.getUserId());
        Team team = objectService.getTeamById(teamId);
        if (team.getIsDefaultTeam()) {
            throw new ResourceException(HttpStatus.FORBIDDEN, "Can't edit members of a default team");
        }
        String approveUrl = String.format("http://localhost:3000/approveTeamJoin/%s/%s", team.getUuid(), user.getUuid());
        emailService.sendRegistrationMail(
                team.getAdmin().getEmail(),
                "New user wants to join the team.",
                String.format(
                        "User '%s' wants to join your team '%s'. Click here to approve them: %s",
                        user.getUsername(),
                        team.getName(),
                        approveUrl
                )
        );
        team.addPendingMember(user);
        return new ResponseEntity<>(ObjectMapperUtil.map(teamRepository.save(team), TeamDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<TeamDTO> leave(TeamJoinDetails details, Long teamId) {
        User user = objectService.getUserById(details.getUserId());
        Team team = objectService.getTeamById(teamId);
        if (team.getIsDefaultTeam()) {
            throw new ResourceException(HttpStatus.FORBIDDEN, "Can't edit members of a default team");
        }
        team.removeMember(user);
        return new ResponseEntity<>(ObjectMapperUtil.map(teamRepository.save(team), TeamDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<HttpStatus> approveUser(UserApprovalDetails details) {
        User groupAdmin = objectService.getUserByJwt();
        Team team = objectService.getTeamByUuid(details.getTeamUuid());
        User user = objectService.getUserByUuid(details.getUserUuid());
        if (!team.getAdmin().equals(groupAdmin)) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "You are not team admin!!!");
        }
        team.removePendingMember(user);
        team.addMember(user);
        teamRepository.save(team);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<TeamDTO> getByUuid(String uuid) {
        Team team = objectService.getTeamByUuid(uuid);
        return new ResponseEntity<>(ObjectMapperUtil.map(team, TeamDTO.class), HttpStatus.OK);
    }
}
