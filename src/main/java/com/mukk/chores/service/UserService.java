package com.mukk.chores.service;

import com.mukk.chores.dto.BasicUserDTO;
import com.mukk.chores.dto.UserDTO;
import com.mukk.chores.model.User;
import com.mukk.chores.repository.UserRepository;
import com.mukk.chores.util.ObjectMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;
    private final ObjectService objectService;

    public ResponseEntity<UserDTO> getByID(Long id) {
        User user = objectService.getUserById(id);
        return new ResponseEntity<>(ObjectMapperUtil.map(user, UserDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<List<UserDTO>> getAll() {
        return new ResponseEntity<>(ObjectMapperUtil.mapAll(userRepository.findAll(), UserDTO.class), HttpStatus.OK);
    }

    public ResponseEntity<BasicUserDTO> getByUuid(String uuid) {
        User user = objectService.getUserByUuid(uuid);
        return new ResponseEntity<>(ObjectMapperUtil.map(user, BasicUserDTO.class), HttpStatus.OK);
    }
}
