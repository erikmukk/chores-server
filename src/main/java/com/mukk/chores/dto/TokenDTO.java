package com.mukk.chores.dto;

import lombok.Data;
import org.springframework.lang.NonNull;

@Data
public class TokenDTO {

    @NonNull
    String token;
}
