package com.mukk.chores.dto;

import lombok.Data;

import java.util.List;

@Data
public class TeamDTO {
    Long id;
    String uuid;
    String name;
    String description;
    UserDTO admin;
    UserDTO createdBy;
    List<BasicUserDTO> members;
    Boolean isDefaultTeam;
    List<BasicUserDTO> pendingUsers;
}
