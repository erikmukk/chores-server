package com.mukk.chores.dto;

import lombok.Data;
import org.springframework.lang.NonNull;

@Data
public class StatisticsDTO {
    @NonNull
    Integer totalChores;
    @NonNull
    Integer completedChores;
    @NonNull
    Integer inProgressChores;
    @NonNull
    Integer createdChores;
    @NonNull
    Integer assignedChores;
    @NonNull
    Integer teams;
}
