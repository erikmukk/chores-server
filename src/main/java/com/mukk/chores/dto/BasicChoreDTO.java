package com.mukk.chores.dto;

import com.mukk.chores.model.enums.ChoreStatus;
import lombok.Data;

@Data
public class BasicChoreDTO {
    Long id;
    String name;
    String description;
    ChoreStatus choreStatus;
}
