package com.mukk.chores.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class BasicUserDTO {
    Long id;
    String uuid;
    String username;
    String firstName;
    String lastName;
}
