package com.mukk.chores.dto;

import com.mukk.chores.model.Team;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class UserDTO {
    Long id;
    String uuid;
    String username;
    String email;
    String firstName;
    String lastName;
    List<Team> pendingTeams;
}
