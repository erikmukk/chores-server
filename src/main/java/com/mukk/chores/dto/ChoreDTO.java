package com.mukk.chores.dto;

import com.mukk.chores.model.enums.ChoreStatus;
import lombok.Data;

@Data
public class ChoreDTO {
    Long id;
    String name;
    String description;
    BasicUserDTO createdBy;
    BasicUserDTO assignedTo;
    ChoreStatus choreStatus;
}
