package com.mukk.chores.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ResourceException extends RuntimeException {
    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public ResourceException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
