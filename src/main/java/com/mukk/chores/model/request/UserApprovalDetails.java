package com.mukk.chores.model.request;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserApprovalDetails {
    @NonNull
    private final String userUuid;

    @NonNull
    private final String teamUuid;
}
