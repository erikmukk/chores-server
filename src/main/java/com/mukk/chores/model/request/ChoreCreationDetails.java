package com.mukk.chores.model.request;

import lombok.Data;
import org.springframework.lang.NonNull;

@Data
public class ChoreCreationDetails {

    @NonNull
    private final String name;
    @NonNull
    private final String description;
    @NonNull
    private final Long assignedTo;
    @NonNull
    private final Long targetTeam;

}
