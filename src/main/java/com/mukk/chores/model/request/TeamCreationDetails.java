package com.mukk.chores.model.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

@NoArgsConstructor
@Data
public class TeamCreationDetails {

    @NonNull
    private String name;

    @NonNull
    private String description;
}
