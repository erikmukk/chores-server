package com.mukk.chores.model.request;

import com.mukk.chores.model.enums.ChoreStatus;
import lombok.Data;

@Data
public class ChoreEditDetails {
    private final ChoreStatus choreStatus;
    private final Long assignedTo;
    private final String name;
    private final String description;
}
