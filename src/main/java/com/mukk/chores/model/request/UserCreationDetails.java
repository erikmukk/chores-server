package com.mukk.chores.model.request;

import lombok.Data;
import org.springframework.lang.NonNull;

@Data
public class UserCreationDetails {
    @NonNull
    private final String username;
    @NonNull
    private final String email;
    @NonNull
    private final String password;
    @NonNull
    private final String firstName;
    @NonNull
    private final String lastName;
}
