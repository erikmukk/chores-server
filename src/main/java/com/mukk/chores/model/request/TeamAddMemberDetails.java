package com.mukk.chores.model.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

@Data
@NoArgsConstructor
public class TeamAddMemberDetails {

    @NonNull
    private Long userID;
}
