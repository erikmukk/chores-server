package com.mukk.chores.model;

import com.fasterxml.uuid.Generators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Builder.Default
    private String uuid = Generators.timeBasedGenerator().generate().toString();

    private String name;

    private String description;

    @Builder.Default
    private Boolean isDefaultTeam = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by_user_id", referencedColumnName = "id")
    private User createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "admin_id", referencedColumnName = "id")
    private User admin;

    @ManyToMany(fetch = FetchType.LAZY,
        cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE})
    @JoinTable(name = "team_user",
        joinColumns = { @JoinColumn(name = "team")},
            inverseJoinColumns = { @JoinColumn(name = "user")}
    )
    @Builder.Default
    private List<User> members = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE})
    @JoinTable(name = "team_pending_user",
            joinColumns = { @JoinColumn(name = "team")},
            inverseJoinColumns = { @JoinColumn(name = "user")}
    )
    @Builder.Default
    private List<User> pendingUsers = new ArrayList<>();

    @OneToMany(mappedBy = "targetTeam", fetch = FetchType.LAZY)
    private List<Chore> chores;

    public void addPendingMember(User user) {
        pendingUsers.add(user);
        user.getPendingTeams().add(this);
    }

    public void removePendingMember(User user) {
        pendingUsers.remove(user);
        user.getPendingTeams().remove(this);
    }

    public void addMember(User user) {
        if (!members.contains(user)) {
            members.add(user);
            user.getTeams().add(this);
        }
    }

    public void removeMember(User user) {
        if (members.contains(user)) {
            members.remove(user);
            user.getTeams().remove(this);
        }
    }
}
