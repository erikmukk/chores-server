package com.mukk.chores.model.enums;

public enum ChoreStatus {
    NOT_STARTED,
    STARTED,
    COMPLETED,
}
