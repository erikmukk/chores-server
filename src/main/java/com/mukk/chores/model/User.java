package com.mukk.chores.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.uuid.Generators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name="user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Builder.Default
    private String uuid = Generators.timeBasedGenerator().generate().toString();

    private String username;

    private String email;

    private String firstName;

    private String lastName;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Role> roles;

    @JsonIgnore
    private String passwordHash;

    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Chore> createdChores;

    @OneToMany(mappedBy = "assignedTo", fetch = FetchType.LAZY)
    private List<Chore> assignedChores;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "members")
    private List<Team> teams;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "pendingUsers")
    private List<Team> pendingTeams;

    private void addTeam(Team team) {
        teams.add(team);
        team.getMembers().add(this);
    }

    private void removeTeam(Team team) {
        teams.remove(team);
        team.getMembers().remove(this);
    }
}
